package com.kgc.cn.provider.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by boot on 2019/12/5.
 */
@Configuration
@MapperScan("com.kgc.cn.provider.mapper")
public class MybatisConfig {
}
