package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Maps;
import com.kgc.cn.common.model.Goods;
import com.kgc.cn.common.model.GoodsExample;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.OrderAndGoods;
import com.kgc.cn.common.model.OrderAndGoodsExample;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.vo.GoodsQueryVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.OrderAndGoodsMapper;
import com.kgc.cn.provider.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private OrderAndGoodsMapper orderAndGoodsMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Override
    public GoodsQueryVo showDetails(String gid) {
        return goodsMapper.queryGoodsById(gid).toGoodsQueryVo();

    }

    @Override
    public Map<String, Map<String, List<GoodsQueryVo>>> showAll() {
        Map<String, Map<String, List<GoodsQueryVo>>> maps = Maps.newHashMap();
        List<String> typeLists = goodsMapper.queryType();
        typeLists.forEach(type -> {
            List<String> propertyList = goodsMapper.queryProperty(type);
            Map<String, List<GoodsQueryVo>> map = Maps.newHashMap();
            propertyList.forEach(property -> {
                List<Goods> goodsList = goodsMapper.queryGoodsByProperty(property);
                List<GoodsQueryVo> goodsQueryVoList = goodsList.stream().map(eachGoods ->
                        eachGoods.toGoodsQueryVo()).collect(Collectors.toList());
                map.put(property, goodsQueryVoList);
            });
            maps.put(type, map);
        });
        return maps;
    }

    @Override
    public void updateStock(String gid, Integer count) {
        Goods goods = goodsMapper.selectByPrimaryKey(gid);
        goods.setgStock(goods.getgStock() - count);
        goodsMapper.updateByPrimaryKeySelective(goods);
    }

    public void updateSales(String gid, Integer count) {
        Goods goods = goodsMapper.selectByPrimaryKey(gid);
        goods.setgSalesVolume(goods.getgSalesVolume() + count);
        goodsMapper.updateByPrimaryKeySelective(goods);
    }

    @Override
    public List<OrderAndGoods> getGoodsAndCounts(String oid) {
        OrderAndGoodsExample orderAndGoodsExample = new OrderAndGoodsExample();
        orderAndGoodsExample.createCriteria().andOIdEqualTo(oid);
        return orderAndGoodsMapper.selectByExample(orderAndGoodsExample);
    }

    @Override
    public void updateStatus(String oid, int stu) {
        Order order = new Order(oid,stu);
        orderMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public List<Goods> goodsNum() {
        GoodsExample goodsExample = new GoodsExample();
        return goodsMapper.selectByExample(goodsExample);
    }
}
