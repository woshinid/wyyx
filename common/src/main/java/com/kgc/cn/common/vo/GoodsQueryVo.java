package com.kgc.cn.common.vo;

import com.kgc.cn.common.model.Goods;
import com.kgc.cn.common.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

@ApiModel(value = "商品查询用model")
public class GoodsQueryVo implements Serializable {

    @ApiModelProperty(value = "商品id")
    private String gId;

    @ApiModelProperty(value = "商品名")
    private String gName;

    @ApiModelProperty(value = "商品价格")
    private Long gPrice;

    @ApiModelProperty(value = "商品库存")
    private Integer gStock;

    @ApiModelProperty(value = "商品封面图片")
    private String gCover;

    @ApiModelProperty(value = "商品图片集合")
    private List<String> pictureList;

    @ApiModelProperty(value = "商品类型")
    private String gProperty;

    @ApiModelProperty(value = "商品销量")
    private Long gSalesVolume;

    @ApiModelProperty(value = "打折情况")
    private DiscountVo discountVo;

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public Long getgPrice() {
        return gPrice;
    }

    public void setgPrice(Long gPrice) {
        this.gPrice = gPrice;
    }

    public Integer getgStock() {
        return gStock;
    }

    public void setgStock(Integer gStock) {
        this.gStock = gStock;
    }

    public String getgCover() {
        return gCover;
    }

    public void setgCover(String gCover) {
        this.gCover = gCover;
    }

    public List<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<String> pictureList) {
        this.pictureList = pictureList;
    }

    public String getgProperty() {
        return gProperty;
    }

    public void setgProperty(String gProperty) {
        this.gProperty = gProperty;
    }

    public Long getgSalesVolume() {
        return gSalesVolume;
    }

    public void setgSalesVolume(Long gSalesVolume) {
        this.gSalesVolume = gSalesVolume;
    }

    public DiscountVo getDiscountVo() {
        return discountVo;
    }

    public void setDiscountVo(DiscountVo discountVo) {
        this.discountVo = discountVo;
    }
    public Goods toGoods(){
        ConvertUtil<GoodsQueryVo,Goods> convertUtil=new ConvertUtil<>(Goods.class);
        return convertUtil.Convert(this);
    }
}
