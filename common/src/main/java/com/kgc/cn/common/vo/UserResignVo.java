package com.kgc.cn.common.vo;

import com.kgc.cn.common.model.User;
import com.kgc.cn.common.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;

import java.io.Serializable;


@ApiModel(value = "用户注册用Vo")
public class UserResignVo implements Serializable {

    @ApiModelProperty(value = "手机号", required = true)
    private String uPhone;

    @ApiModelProperty(value = "密码", required = true)
    private String uPassword;

    @ApiModelProperty(value = "昵称")
    private String uNickName;

    @ApiModelProperty(value = "验证码", required = true)
    private String check;

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }

    public String getuNickName() {
        return uNickName;
    }

    public void setuNickName(String uNickName) {
        this.uNickName = uNickName;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public User toUser() {
        ConvertUtil<UserResignVo, User> convertUtil = new ConvertUtil<>(User.class);
        return convertUtil.Convert(this);
    }
}
