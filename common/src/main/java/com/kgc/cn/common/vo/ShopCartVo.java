package com.kgc.cn.common.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.io.Serializable;

@ApiModel(value = "购物车model")
public class ShopCartVo implements Serializable {

    @ApiModelProperty(value = "商品id")
    private String gId;

    @ApiModelProperty(value = "商品数量")
    private Integer gCount;


    public ShopCartVo() {
    }

    public ShopCartVo(String gId, Integer gCount) {
        this.gId = gId;
        this.gCount = gCount;
    }

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public Integer getgCount() {
        return gCount;
    }

    public void setgCount(Integer gCount) {
        this.gCount = gCount;
    }
}
