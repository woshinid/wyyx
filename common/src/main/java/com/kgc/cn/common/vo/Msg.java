package com.kgc.cn.common.vo;

import lombok.Builder;
import lombok.Data;

/**
 * @Date 2019/12/25 13:52
 * @Creat by Crane
 */
@Data
@Builder
public class Msg {
    private String order;
    private long price;

}
