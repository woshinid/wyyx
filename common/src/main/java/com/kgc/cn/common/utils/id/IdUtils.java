package com.kgc.cn.common.utils.id;

import org.apache.http.client.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class IdUtils {
    public static String getUId(String phone) {
        return DateUtils.formatDate(new Date(), "yyMMddHHmmss") + phone.substring(7)
                + UUID.randomUUID().toString().replace("-", "").substring(0, 4);
    }

    public static String queryOid() {
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String uid = UUID.randomUUID().toString().replace("-", "").substring(0, 16);
        String oid = time + uid;
        return oid;
    }
}
