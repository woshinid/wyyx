package com.kgc.cn.common.utils.queryid;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Date 2019/12/21 11:52
 * @Creat by Crane
 */
public class QueryId {


    public static String queryOid(String phone) {
        String ph = phone.substring(phone.length() - 4);
        String time = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String uid = UUID.randomUUID().toString().replace("-", "").substring(0, 12);
        String oid = ph + time + uid;
        return oid;
    }
}
