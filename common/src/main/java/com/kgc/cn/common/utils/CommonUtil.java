package com.kgc.cn.common.utils;

import java.util.UUID;

/**
 * @Date 2019/12/10 16:21
 * @Creat by Crane
 */
public class CommonUtil {
    public static String generateUUID(){
        return UUID.randomUUID().toString().replaceAll("-","").substring(0,32);
    }
}
