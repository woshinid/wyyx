package com.kgc.cn.common.service;

import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.vo.UserLoginVo;
import com.kgc.cn.common.vo.UserResignVo;

import java.util.Map;

public interface UserService {
    /**
     * 根据openid尝试获得用户信息
     *
     * @param openId
     * @return
     */
    User checkWxLogin(String openId);

    /**
     * 根据手机号查询用户
     *
     * @param phone
     * @return
     */
    User queryUserByPhone(String phone);

    /**
     * 微信绑定
     */
    User insertWxUser(String phone, String nickName, String openId);

    /**
     * 登录
     */
    User login(UserLoginVo userLoginVo) throws Exception;

    /**
     * 注册
     */
    User register(UserResignVo userResignVo) throws Exception;

    /**
     * 从购物车中搜索订单相关信息
     * @param uid
     * @param countMap
     */
    Order queryOrders(String uid, Map<String,Integer> countMap, String oid);

    /**
     * 获取订单中的商品列表
     * @param gid
     * @param uid
     * @return
     */
    Map<String,Integer> selCount(String gid,String uid);

    User queryUserById(String uid);
}
