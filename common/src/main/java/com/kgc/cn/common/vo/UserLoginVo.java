package com.kgc.cn.common.vo;

import com.kgc.cn.common.model.User;
import com.kgc.cn.common.utils.conver.ConvertUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "用户登录用model")
public class UserLoginVo implements Serializable {

    @ApiModelProperty(value = "手机号", required = true)
    private String uPhone;

    @ApiModelProperty(value = "密码", required = true)
    private String uPassword;

    public String getuPhone() {
        return uPhone;
    }

    public void setuPhone(String uPhone) {
        this.uPhone = uPhone;
    }

    public String getuPassword() {
        return uPassword;
    }

    public void setuPassword(String uPassword) {
        this.uPassword = uPassword;
    }
    public User toUser() {
        ConvertUtil<UserLoginVo, User> convertUtil = new ConvertUtil<>(User.class);
        return convertUtil.Convert(this);
    }
}
