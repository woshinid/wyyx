package com.kgc.cn.common.vo;

import com.kgc.cn.common.model.Goods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;

import java.io.Serializable;
@Builder
@ApiModel(value = "购物车列表")
public class ShopCartShowVo implements Serializable {

    @ApiModelProperty(value = "商品数量")
    private Integer gCount;

    @ApiModelProperty(value = "商品信息")
    private Goods goods;

    public Integer getgCount() {
        return gCount;
    }

    public void setgCount(Integer gCount) {
        this.gCount = gCount;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
