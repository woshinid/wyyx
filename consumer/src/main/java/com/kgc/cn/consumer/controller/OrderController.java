package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.model.Order;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.service.OrderService;
import com.kgc.cn.common.service.UserService;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.common.utils.wx.WxPay;
import com.kgc.cn.consumer.config.aop.CurrentUser;
import com.kgc.cn.consumer.config.aop.UserLoginRequired;
import com.kgc.cn.consumer.utils.activemq.ActiveMqUtils;
import com.kgc.cn.common.utils.id.IdUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import com.kgc.cn.consumer.utils.stock.StockUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Date 2019/12/23 14:05
 * @Creat by Crane
 */

@RestController
@Api(tags = "下单")
@RequestMapping("/order")
public class OrderController {
    @Reference
    private OrderService orderService;

    @Reference
    private UserService userService;

    @Reference
    private GoodsService goodsService;

    @Autowired
    private ActiveMqUtils activeMqUtils;

    @Autowired
    private WxPay wxPay;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private StockUtils stockUtils;


    @UserLoginRequired
    @PostMapping("/buy")
    @ApiOperation(value = "买")
    public ReturnResult buy(@CurrentUser User user,
                            @ApiParam("商品id") @RequestParam String gid,
                            @ApiParam("商品数量") @RequestParam int count) throws Exception {
        Order order = orderService.queryOrder(user.getuId(), gid, count);
        if (!ObjectUtils.isEmpty(order)) {
            redisUtils.set(RedisEnums.ORDER_COMMON + order.getoId(), 1, 86400);
            redisUtils.decr(RedisEnums.STOCK_NAMESPACE + gid, count);
            Map<String, String> map = wxPay.codeUrl(order);
            return ReturnResultUtils.returnSuccess(map);
        }
        return ReturnResultUtils.returnFail(Enums.OrderEnum.ORDER_ADD_FAIL);
    }

    @UserLoginRequired
    @PostMapping("/cleanShopCart")
    @ApiOperation(value = "买买买")
    public ReturnResult payAll(@CurrentUser User user,
                               @ApiParam("商品id") @RequestParam String gid) throws Exception {
        Map<String,Integer> countmap = userService.selCount(gid,user.getuId());
        String oid = IdUtils.queryOid();
        stockUtils.decrStock(countmap);
        Map<String, String> map = wxPay.codeUrl(userService.queryOrders(user.getuId(),countmap,oid));
        return ReturnResultUtils.returnSuccess(map);
    }

    @Transactional
    @UserLoginRequired
    @ApiOperation("抢购")
    @GetMapping(value = "/rushBuy")
    public ReturnResult rushBuy(@ApiParam("商品id") @RequestParam String gid, @CurrentUser User user) throws Exception {
        if ((int) redisUtils.get(RedisEnums.STOCK_NAMESPACE + gid) <= 0) {
            return ReturnResultUtils.returnFail(Enums.OrderEnum.STOCK_FAIL);
        }
        if (redisUtils.checkSpeed(user.getuId(), 1, 60 * 60)) {
            Order order = orderService.rushOrder(user.getuId(), gid);
            redisUtils.set(RedisEnums.RUSH_ORDER + order.getoId(), order, 60 * 5);
            activeMqUtils.sendMsgByQueue(RedisEnums.RUSH_ORDER, order);
            if (redisUtils.lock(RedisEnums.ORDER_RUSH, order, 1)) {
                redisUtils.decr(RedisEnums.STOCK_NAMESPACE + gid, 1);
            }
            redisUtils.delLock(RedisEnums.ORDER_RUSH);
            Map<String, String> map = wxPay.codeUrl(order);
            return ReturnResultUtils.returnSuccess(map);
        }
        return ReturnResultUtils.returnFail(Enums.OrderEnum.RUSH_FAIL);
    }
}
