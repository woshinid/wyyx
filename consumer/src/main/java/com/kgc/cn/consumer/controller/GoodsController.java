package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.Enums;
import com.kgc.cn.common.model.Goods;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.utils.returns.ReturnResult;
import com.kgc.cn.common.utils.returns.ReturnResultUtils;
import com.kgc.cn.common.vo.GoodsQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "商品操作")
@RequestMapping(value = "/user")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @ApiOperation(value = "展示商品详情")
    @PostMapping(value = "/showDetail")
    public ReturnResult<GoodsQueryVo> showDetail(@RequestParam String gid) {
        GoodsQueryVo goodsQueryVo = goodsService.showDetails(gid);
        if (goodsQueryVo != null) {
            return ReturnResultUtils.returnSuccess(goodsQueryVo);
        }
        return ReturnResultUtils.returnFail(Enums.GoodsEnum.NONE_ERROR);
    }
    @ApiOperation(value = "展示全部商品")
    @GetMapping(value = "/showAll")
    public ReturnResult<Map<String, Map<String, List<GoodsQueryVo>>>> showAll(){
        return ReturnResultUtils.returnSuccess(goodsService.showAll());
    }
}
