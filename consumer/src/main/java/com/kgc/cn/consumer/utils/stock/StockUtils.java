package com.kgc.cn.consumer.utils.stock;

import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @Date 2019/12/25 17:01
 * @Creat by Crane
 */
@Component
public class StockUtils {
    @Autowired
    private RedisUtils redisUtils;

    public void decrStock(Map<String,Integer> map){
            map.forEach((gid,count)->{
            redisUtils.decr(RedisEnums.STOCK_NAMESPACE + gid,count);
        });
    }
}
