package com.kgc.cn.consumer.listener;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.enums.RedisEnums;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.transaction.annotation.Transactional;

public class KeyExpiredListener extends KeyExpirationEventMessageListener {
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private GoodsService goodsService;

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Transactional
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String[] key = message.toString().split(":");
        if ("commonOrder".equals(key[0]) || "rushOrder".equals(key[0])) {
            String oid = key[1];
            goodsService.getGoodsAndCounts(oid).forEach(goodsAndCount -> {
                goodsService.updateStock(goodsAndCount.getgId(), -goodsAndCount.getoCount());
                redisUtils.incr(RedisEnums.STOCK_NAMESPACE + goodsAndCount.getgId(), goodsAndCount.getoCount());
            });
            goodsService.updateStatus(oid, -1);
        }

    }
}
