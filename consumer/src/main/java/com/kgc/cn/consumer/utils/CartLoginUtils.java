package com.kgc.cn.consumer.utils;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.model.ShopCart;
import com.kgc.cn.common.model.User;
import com.kgc.cn.common.service.ShopCartService;
import com.kgc.cn.consumer.utils.ip.IpUtils;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class CartLoginUtils {

    @Reference
    private ShopCartService shopCartService;

    @Autowired
    private RedisUtils redisUtils;

    @Transactional
    public void addShopCartLogin(User user, HttpServletRequest request) {
        String ip = IpUtils.getIpAddress(request);
        Set<String> keysSet = redisUtils.keys(ip + "*");
        keysSet.forEach(key -> {
            String gId = key.substring(key.indexOf(":") + 1);
            int goodsNum = (int) redisUtils.get(key);
            String uid = user.getuId();
            ShopCart shopCart = ShopCart.builder().uId(uid).gId(gId).gCount(goodsNum).build();
            shopCartService.addShopCartLogin(shopCart);
            redisUtils.del(key);
        });

    }
}
