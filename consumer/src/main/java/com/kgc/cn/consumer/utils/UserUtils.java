package com.kgc.cn.consumer.utils;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.consumer.utils.redis.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class UserUtils {

    @Autowired
    private RedisUtils redisUtils;

    public static String getNickName(Object object) {
        return JSONObject.parseObject(object.toString()).getString("nickname");
    }
}
